import java.util.Scanner;

public class BoardGameApp{
	public static void main(String [] args){
		Scanner reader = new Scanner(System.in);
		
		Board gameBoard = new Board();
		System.out.println("Welcome to Caste-Wall-Game");
		
		//set the amount of Castles to place and the inital value of turns
		int numCastles = 7;
		int turns = 0;
		//runs the game while all castles has not been placed and while the maximum amount of turns has not been reached
		while(numCastles > 0 && turns < 8){
			System.out.println(gameBoard);
			System.out.println("Numbers of Castle to place: " +numCastles);
			System.out.println("Current numbers of turns: " +turns + "\n");
			
		//asks the player the coordinate where he would like to place his castles
			System.out.println("Player enter which row (0-4) you would like to place your Castle.");
			int rowInput = reader.nextInt();
			
			System.out.println("Player enter which column (0-4) you would like to place your Castle.");
			int colInput = reader.nextInt();
			
		// if user entered an invalid coordinate game will continue to ask him for a valid coordinate
			int tokenCoordinate = gameBoard.placeToken(rowInput,colInput);
			while(tokenCoordinate < 0){
				System.out.println("The input is invalid. Player, re-enter which row (0-4) you would like to place your Castle.");
				rowInput = reader.nextInt();
			
				System.out.println("The input is invalid. Player, re-enter which col (0-4) you would like to place your Castle.");
				colInput = reader.nextInt();
				 
				tokenCoordinate = gameBoard.placeToken(rowInput,colInput);
			}
			
		//Informs the user if the castles has been successfully placed or not
			if(tokenCoordinate == 1){
				System.out.println("There is a wall at (" + rowInput + ", " + colInput +")" + "\n" );
				turns++;
			}else if(tokenCoordinate == 0){
				System.out.println("Castle has been successfully placed at (" + rowInput + ", " + colInput +")" );
				turns++;
				numCastles--;
			}
		}
		
		//Tells the user if they won or lost
		System.out.println(gameBoard);
		if(numCastles == 0){
			System.out.println("You Won!");
		}else {
			System.out.println("You Lost");
		}
	}
}