public enum Tile{

	BLANK("_"),
	WALL("W"),
	HIDDEN_WALL("H"),
	CASTLE("C");
	
	private final String name;
	
	private Tile(final String name)
	{
		this.name = name;
	}
		
	public String getName()
	{
		return this.name;
	}
}