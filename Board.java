import java.util.Random;

public class Board{

	private Tile[][] grid;
	private final int SIZE;

	//Builts the Board with BLANK and HIDDEN_WALL 
	public Board()
	{
		this.SIZE = 5;
		this.grid = new Tile[SIZE][SIZE];
		Random rng = new Random();
		
		for (int i = 0; i < this.grid.length; i++){
			int rngIndex = rng.nextInt(grid[i].length);
			for(int j = 0; j <this.grid[i].length; j++){
				this.grid[i][j] = Tile.BLANK;
				
				if(j == rngIndex) {
					grid[i][j] = Tile.HIDDEN_WALL;
				}
			}
		}
	}
	//Makes sure that the Board is displayed in a 5x5 format in "_"
	public String toString()
	{	
		String output ="";
			for (int i = 0; i < this.grid.length; i++){
				for(int j = 0; j <this.grid[i].length; j++){
					output += this.grid[i][j].getName()+" ";
				}
				output += "\n";
			}
		return output;
	}
	
	/*
	-Takes int row and int col as parameters
	-Checks if parameters are valid and if theres a castles,a wall or nothing at the coordinates
	*/
	public int placeToken(int row, int col)
	{
		if (!(row < this.SIZE &&  row >= 0 &&  col < this.SIZE &&  col >=0)){
			return -2;
		}
		else if(this.grid[row][col].equals(Tile.CASTLE) || this.grid[row][col].equals(Tile.WALL)){
			return -1;
		}
		else if(this.grid[row][col].equals(Tile.HIDDEN_WALL)){
			this.grid[row][col] = Tile.WALL;
			return 1;
		}
		else{
			this.grid[row][col] = Tile.CASTLE;
			return 0;
		}
	}
	
	


}